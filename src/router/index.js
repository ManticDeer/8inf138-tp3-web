import Vue from 'vue';
import VueRouter from "vue-router";

import Home from "@/components/pages/home/index.vue"
import MainMenu from "@/components/pages/MainMenu/index.vue"
import ChangeMdp from "@/components/pages/changeMdp/index.vue"
import AdminPanel from "@/components/pages/adminPanel/index.vue"
import clientResidentiel from '@/components/pages/clientResidentiel/index.vue'
import clientAffaire from '@/components/pages/clientAffaire/index.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/Menu',
        name: 'mainmenu',
        component: MainMenu
    },
    {
        path:"/passwordChange",
        name:"changemdp",
        component: ChangeMdp
    },
    {
        path:"/adminPanel",
        name: "adminpanel",
        component:AdminPanel
    },
    {
        path:"/residentiel",
        name:'residentiel',
        component: clientResidentiel
    },
    {
        path:"/affaire",
        name:'affaire',
        component: clientAffaire
    }
];

const router = new VueRouter({
    routes,
    mode:'history'
});

export default router;
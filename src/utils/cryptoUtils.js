import cryptoJS from 'crypto-js'


//TIRÉ/COPIÉ DE LA DOCUMENTATION DE https://cryptojs.gitbook.io/docs/#ciphers 
const JSONFormatter = {
    clef: "maClef",

    stringify: function(cipherParams) {
        // create json object with ciphertext
        var jsonObj = { ct: cipherParams.ciphertext.toString(cryptoJS.enc.Base64) };
        // optionally add iv or salt
        if (cipherParams.iv) {
          jsonObj.iv = cipherParams.iv.toString();
        }
        if (cipherParams.salt) {
          jsonObj.s = cipherParams.salt.toString();
        }
        // stringify json object
        return JSON.stringify(jsonObj);
      },

    parse: function(jsonStr) {
        // parse json string
        var jsonObj = JSON.parse(jsonStr);
        // extract ciphertext from json object, and create cipher params object
        var cipherParams = cryptoJS.lib.CipherParams.create({
          ciphertext: cryptoJS.enc.Base64.parse(jsonObj.ct)
        });
        // optionally extract iv or salt
        if (jsonObj.iv) {
          cipherParams.iv = cryptoJS.enc.Hex.parse(jsonObj.iv);
        }
        if (jsonObj.s) {
          cipherParams.salt = cryptoJS.enc.Hex.parse(jsonObj.s);
        }
        return cipherParams;
    }
};

export default JSONFormatter;
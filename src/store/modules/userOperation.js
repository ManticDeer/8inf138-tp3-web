const state = {
    role: 3,
    loggedIn: false,
    loggedUsername : " ",
    loginToken: " ",
};

const mutations = {
    setLoggedIn(state, tokenRecu){
        state.loggedIn = true;
        state.loginToken = tokenRecu;
    },
    notLoggedIn(state){
        state.loginToken=" ";
        state.loggedIn=false;
        state.loggedUsername="";
        state.role = 3;
    },

    setUser(state, userRecu){
        state.loggedUsername = userRecu;
    },

    setRole(state, roleRecu){
        state.role = roleRecu;
    }
};

const actions = {
    setToken(context, tokenRecu){
        context.commit("setLoggedIn", tokenRecu);
    },
    noToken(context){
        context.commit("notLoggedIn");
    },

    setUsername(context, usernameRecu){
        context.commit("setUser", usernameRecu);
    },

    setRole(context, roleRecu){
        context.commit("setRole", roleRecu);
    }
};

export default{
    namespaced:true,
    state,
    actions,
    mutations
};
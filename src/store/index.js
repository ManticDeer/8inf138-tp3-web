import Vue from "vue"
import Vuex from "vuex"

import userOperation from "@/store/modules/userOperation.js"

Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        userOperation
    },
});